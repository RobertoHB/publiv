<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
 

$this->title = "Establecimientos";

//$this->params['breadcrumbs'][] = ['label' => 'Autores', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//\yii\web\YiiAsset::register($this);

$options = ['style' => ['width' => '290px', 'height' => '200px']];

?>


<div class="establecimientos-view">
   
 
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="caption">
          
          <div style=""> <?= Html::img("@web/img_colaboradores/".$model['id'].".jpg", $options)?></div>
       
        <h3><?= $model['nombre']?></h3>
        <p><?= $model['dir']?></p>
        <p><?= $model['pob'] ?></p>
        <?php
        if(isset($model['movil'])){?>
        <p><?= $model['movil'] ?></p>
        <?php
        }
        ?>
   
        <?= Html::a('Ver Establecimiento', ['colaboradores/establecimientos'], ['class' => 'btn btn-primary btn-md', 'style'=>'margin-top:10px;']) ?>
       
      </div>
    </div>
  </div>
    
</div>