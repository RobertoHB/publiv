<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;
use app\models\filtroEstable;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;


$this->registerCssFile('@web/css/filtros.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);

?>

   
<div class="jumbotron-fluid text-center bg-success">
  <h1>Listado de Establecimientos</h1>
  
</div>

<div class="container" style="margin-left:-200px;margin-top:50px;border-radius:5px;border:1px solid grey;width: 275px;display:flex;flex-direction: column;justify-content: space-around;">
<!--<div class="establecimientos-view">-->

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="form-group col-md-12" style="margin-top: -15px;">
        
        <?php $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'formulario',
            'enableClientValidation' => true,
            'enableAjaxValidation' => false,
            'action'=>'establecimientos',
           ]); ?>
           

   
       <?= $form->field($model,'tipoEstablecimiento')->dropDownList(
               array(''=>'','Bar'=>'Bar','Cafetería'=>'Cafetería','Restaurante'=>'Restaurante','Pub'=>'Pub'),
               $options=['class'=>'form-control form-control-lg', 'style'=>'width:200px'])->label('Tipo Etablecimiento');?>

        <?= $form->field($model,'tipoCliente')->dropDownList(
                array(''=>'','Estudiantes'=>'Estudiantes','Familias'=>'Familias','Jubilados'=>'Jubilados'),
                $options=['class'=>'form-control form-control-lg', 'style'=>'width:200px'])->label('Tipo Cliente');?>
            
        <?= $form->field($model,'afluencia')->dropDownList(
                array(''=>'','Mañanas'=>'Mañanas','Tardes'=>'Tardes','Noches'=>'Noches'),
                $options=['class'=>'form-control form-control-lg', 'style'=>'width:200px'])->label('Afluencia');?>
        
        
        <?= $form->field($model,'provincia')->dropDownList(
                array(''=>'','Alava'=>'Alava','Albacete'=>'Albacete','Alicante'=>'Alicante','Almería'=>'Almería','Asturias'=>'Asturias','Avila'=>'Avila','Badajoz'=>'Badajoz','Barcelona'=>'Barcelona','Burgos'=>'Burgos','Cáceres'=>'Cáceres',
                        'Cádiz'=>'Cádiz','Cantabria'=>'Cantabria','Castellón'=>'Castellón','Ciudad Real'=>'Ciudad Real','Córdoba'=>'Córdoba','La Coruña'=>'La Coruña','Cuenca'=>'Cuenca','Gerona'=>'Gerona','Granada'=>'Granada','Guadalajara'=>'Guadalajara',
                        'Guipúzcoa'=>'Guipúzcoa','Huelva'=>'Huelva','Huesca'=>'Huesca','Islas Baleares'=>'Islas Baleares','Jaén'=>'Jaén','León'=>'León','Lérida'=>'Lérida','Lugo'=>'Lugo','Madrid'=>'Madrid','Málaga'=>'Málaga','Murcia'=>'Murcia','Navarra'=>'Navarra',
                        'Orense'=>'Orense','Palencia'=>'Palencia','Las Palmas'=>'Las Palmas','Pontevedra'=>'Pontevedra','La Rioja'=>'La Rioja','Salamanca'=>'Salamanca','Segovia'=>'Segovia','Sevilla'=>'Sevilla','Soria'=>'Soria','Tarragona'=>'Tarragona',
                        'Santa Cruz de Tenerife'=>'Santa Cruz de Tenerife','Teruel'=>'Teruel','Toledo'=>'Toledo','Valencia'=>'Valencia','Valladolid'=>'Valladolid','Vizcaya'=>'Vizcaya','Zamora'=>'Zamora','Zaragoza'=>'Zaragoza'),
                
                $options=['class'=>'form-control form-control-lg', 'style'=>'width:200px'])->label('Provincias');?>
        
           <?= $form->field($model,'activo')->dropDownList(
                array(''=>'','0'=>'Inactivo','1'=>'Activo'),
                $options=['class'=>'form-control form-control-lg', 'style'=>'width:200px'])->label('Estado');?>
        
    </div>   
        <div class="form-group col-md-12">
            <?= Html::submitButton("filtrar", ["class" => "btn btn-primary","style"=>"width:200px;font-size:20px"]) ?>
    <!--            
           Html::submitButton('Emitir',
                    ['class'=>'btn btn-primary btn-lg active',

                        'style'=>'width:300px',
                        'data-toggle'=>'tooltip',
                        'title'=>Yii::t('app', 'Emisión de Recibos Mensual'),
                    ]
                )-->
        </div>
    <!--</div>-->
    
    
    
 </div>   
    <?php ActiveForm::end(); ?>

<div style="position: relative;top:-490px;padding-top:10px;padding-left:100px;">

    <?php
    if (isset($dataProvider)){
        
//         foreach ($dataProvider->getModels() as $model) {
             echo ListView::widget([
                 'dataProvider' => $dataProvider,
                 'itemView' => '_colaboradores',  
                  
             ]);
}
?>
    
</div>

</body>
</html>

 

 






<!--<div class="well well-sm"><h2 style="text-align: center;max-height: 50px;"></h2></div>-->




