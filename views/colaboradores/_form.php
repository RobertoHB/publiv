<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\colaboradores */
/* @var $form yii\widgets\ActiveForm */
$itemsEstablecimientos =  array('Bar'=>'Bar','Cafetería'=>'Cafetería','Restaurante'=>'Restaurante','Pub'=>'Pub'); 
$itemsClientes =  array('Estudiantes'=>'Estudiantes','Familias'=>'Familias','Jubilados'=>'Jubilados'); 
$itemsAfluencia = array('Mañanas'=>'Mañanas','Tardes'=>'Tardes','Noches'=>'Noches'); 
$itemsProvincias =  array('Alava'=>'Alava','Albacete'=>'Albacete','Alicante'=>'Alicante','Almería'=>'Almería','Asturias'=>'Asturias','Avila'=>'Avila','Badajoz'=>'Badajoz','Barcelona'=>'Barcelona','Burgos'=>'Burgos','Cáceres'=>'Cáceres',
                        'Cádiz'=>'Cádiz','Cantabria'=>'Cantabria','Castellón'=>'Castellón','Ciudad Real'=>'Ciudad Real','Córdoba'=>'Córdoba','La Coruña'=>'La Coruña','Cuenca'=>'Cuenca','Gerona'=>'Gerona','Granada'=>'Granada','Guadalajara'=>'Guadalajara',
                        'Guipúzcoa'=>'Guipúzcoa','Huelva'=>'Huelva','Huesca'=>'Huesca','Islas Baleares'=>'Islas Baleares','Jaén'=>'Jaén','León'=>'León','Lérida'=>'Lérida','Lugo'=>'Lugo','Madrid'=>'Madrid','Málaga'=>'Málaga','Murcia'=>'Murcia','Navarra'=>'Navarra',
                        'Orense'=>'Orense','Palencia'=>'Palencia','Las Palmas'=>'Las Palmas','Pontevedra'=>'Pontevedra','La Rioja'=>'La Rioja','Salamanca'=>'Salamanca','Segovia'=>'Segovia','Sevilla'=>'Sevilla','Soria'=>'Soria','Tarragona'=>'Tarragona',
                        'Santa Cruz de Tenerife'=>'Santa Cruz de Tenerife','Teruel'=>'Teruel','Toledo'=>'Toledo','Valencia'=>'Valencia','Valladolid'=>'Valladolid','Vizcaya'=>'Vizcaya','Zamora'=>'Zamora','Zaragoza'=>'Zaragoza');
?>

<div class="colaboradores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rs')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cp')->textInput() ?>

    <?= $form->field($model, 'pob')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'movil')->textInput() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo')->dropDownList($itemsEstablecimientos, ['prompt' => 'Seleccione Uno' ]); ?>
    
     <?= $form->field($model, 'tipoCliente')->dropDownList($itemsClientes, ['prompt' => 'Seleccione Uno' ]); ?>
    
     <?= $form->field($model, 'provincia')->dropDownList($itemsProvincias, ['prompt' => 'Seleccione Uno' ]); ?>
    
     <?= $form->field($model, 'afluencia')->dropDownList($itemsAfluencia, ['prompt' => 'Seleccione Uno' ]); ?>
    


    
     <?= $form->field($model, 'alta')->widget(DatePicker::className(), [
            // inline too, not bad
            'inline' => false, 
             // modify template for custom rendering
            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-mm-yyyy',
            'todayBtn' => true
            ]
    ]);?>

    

    
     <?= $form->field($model, 'baja')->widget(DatePicker::className(), [
            // inline too, not bad
            'inline' => false, 
             // modify template for custom rendering
            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-mm-yyyy',
            'todayBtn' => true
            ]
    ]);?>

    

    <?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
