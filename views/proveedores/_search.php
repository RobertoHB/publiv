<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedoresSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedores-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'rs') ?>

    <?= $form->field($model, 'dir') ?>

    <?= $form->field($model, 'cp') ?>

    <?php // echo $form->field($model, 'pob') ?>

    <?php // echo $form->field($model, 'movil') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'alta') ?>

    <?php // echo $form->field($model, 'baja') ?>

    <?php // echo $form->field($model, 'observaciones') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
