<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;
use app\models\Users;
use yii\helpers\ArrayHelper;

AppAsset::register($this);
 
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody(); 
 

 ?>

<div class="wrap">
    <?php
//    $obj = new User();
    //$valores = Yii::$app->user->identity->username;
    //$usuario = $obj->getTipo();
    //echo $usuario;
    //$tipoUsuario = 20;
//    $request = Yii::$app()->request;
//    $tipoUsuario = $request->username;
//    
//    echo($tipoUsuario);
   // exit;
//   $usuario = new User();  
// var_dump('Yii::$app->user->identity->username');
// $tipoUsuario = $usuario->getTipo($nombreUsuario); 
   
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
       // !Yii::$app->user->isGuest(var_dump(Yii::$app->user->id)),
 
    ]);

    echo Nav::widget([
        
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [   
            ['label' => 'About', 'url' => ['/site/about'],'visible'=>Yii::$app->user->isGuest || Yii::$app->user->identity->tipo == 10 || Yii::$app->user->identity->tipo == 20],
            ['label' => 'Contact', 'url' => ['/site/contact'],'visible'=>Yii::$app->user->isGuest || Yii::$app->user->identity->tipo == 10 || Yii::$app->user->identity->tipo == 20],
            ['label' => 'Registro', 'url' => ['/site/register'],'visible'=>Yii::$app->user->isGuest || Yii::$app->user->identity->tipo == 10 || Yii::$app->user->identity->tipo == 20],
           // ['label' => 'Agencias', 'url' => ['/agencias']],

            ['label' => 'Agencias', 'url' => ['#'],
                'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                'items' => [
                                 ['label' => 'Agencias', 'url' => ['/agencias']],
                                 ['label' => 'Publicidad', 'url' =>['/publicidad']],
                               
                         ], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->tipo == 10,
            ],
            
           // ['label' => '', 'url' => ['/colaboradores']],
             ['label' => 'Colaboradores', 'url' => ['#'],
              'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
              'items' => [
                                 ['label' => 'Establecimientos', 'url' => ['/colaboradores']],
                                 ['label' => 'Listado', 'url' => ['colaboradores/filtrar']],
            
                        ],'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->tipo == 10,
                 
            ],
            
           // ['label' => 'Clientes', 'url' => ['/clientes']],
             ['label' => 'Clientes', 'url' => ['#'],
              'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
              'items' => [
                                 ['label' => 'Clientes', 'url' => ['/clientes']],
                                 ['label' => 'Compran', 'url' =>['/compran']],
                                
                         ],'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->tipo == 10,
            ],

           // ['label' => 'Productos', 'url' => ['/productos']],
             ['label' => 'Productos', 'url' => ['#'],
              'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
              'items' => [
                                 ['label' => 'Productos', 'url' => ['/productos']],
                                 ['label' => 'Visualiza', 'url' =>['/visualiza']],
                            
                         ],'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->tipo == 10,
            ],

            
             ['label' => 'Proveedores', 'url' => ['#'],
              'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
              'items' => [
                                 ['label' => 'Proveedores', 'url' => ['/proveedores']],
                                 ['label' => 'Fabrican', 'url' =>['/fabrican']],
                               
                         ],'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->tipo == 10,
            ],

           // ['label' => 'Proveedores', 'url' => ['/proveedores']],
            
        
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                             
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
     NavBar::end();

    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y')?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
