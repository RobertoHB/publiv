<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\fabrican */

$this->title = 'Update Fabrican: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fabricans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fabrican-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
