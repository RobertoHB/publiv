<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FabricanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fabricans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fabrican-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Fabrican', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'proveedor',
            'producto',
            'fecha',
            'cantidad',
            //'importe',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
