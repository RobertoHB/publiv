<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\fabrican */

$this->title = 'Create Fabrican';
$this->params['breadcrumbs'][] = ['label' => 'Fabricans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fabrican-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
