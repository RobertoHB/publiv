<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Agencias */
/* @var $form yii\widgets\ActiveForm */
$itemsAgencias =  array('Investigación de mercado'=>'Investigación de mercado','Medios tradicionales'=>'Medios tradicionales','BTL'=>'BTL',
                             'Redes sociales'=>'Redes sociales',' SEO'=>' SEO','Marketing'=>'Marketing','Branding'=>'Branding',
                             'Mobile'=>'Mobile','Inbound marketing'=>'Inbound marketing'); 
?>



<div class="agencias-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rs')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cp')->textInput() ?>

    <?= $form->field($model, 'poblacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'movil')->textInput() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'tipo')->dropDownList($itemsAgencias, ['prompt' => 'Seleccione Uno' ]); ?>
    
   
    <?= $form->field($model, 'alta')->widget(DatePicker::className(), [
            // inline too, not bad
            'inline' => false, 
             // modify template for custom rendering
            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-mm-yyyy',
            'todayBtn' => true
            ]
    ]);?>

    <?= $form->field($model, 'baja')->widget(DatePicker::className(), [
            // inline too, not bad
            'inline' => false, 
             // modify template for custom rendering
            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-mm-yyyy',
            'todayBtn' => true
            ]
    ]);?>

    <?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
