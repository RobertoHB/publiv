<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AgenciasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agencias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agencias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Agencias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'rs',
            'dir',
            'cp',
            //'poblacion',
            //'movil',
            //'email:email',
            //'tipo',
            //'alta',
            //'baja',
            //'observaciones:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
