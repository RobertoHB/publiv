<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\visualiza */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="visualiza-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'producto')->textInput() ?>

    <?= $form->field($model, 'publicidad')->textInput() ?>

    <?= $form->field($model, 'colaborador')->textInput() ?>

    <?= $form->field($model, 'cantidad')->textInput() ?>

    <?= $form->field($model, 'fincontrato')->widget(DatePicker::className(), [
        // inline too, not bad
        'inline' => false, 
         // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
        'autoclose' => true,
        'format' => 'dd-mm-yyyy',
        'todayBtn' => true
        ]
    ]);?>
    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
