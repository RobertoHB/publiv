<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\visualiza */

$this->title = 'Create Visualiza';
$this->params['breadcrumbs'][] = ['label' => 'Visualizas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visualiza-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
