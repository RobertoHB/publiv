<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PublicidadSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publicidad-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'agencia') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?= $form->field($model, 'contenido') ?>

    <?= $form->field($model, 'fecha') ?>

    <?php // echo $form->field($model, 'importe') ?>

    <?php // echo $form->field($model, 'observaciones') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
