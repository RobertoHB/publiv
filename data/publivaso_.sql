﻿DROP DATABASE IF EXISTS publivaso;
CREATE DATABASE publivaso
  CHARACTER SET UTF8
  COLLATE utf8_general_ci;
USE publivaso;

DROP TABLE IF EXISTS agencias;
CREATE OR REPLACE TABLE agencias(
  id int(11) AUTO_INCREMENT,
  nombre varchar(100),
  rs varchar(100),
  dir varchar(200),
  cp int(11),
  poblacion varchar(100),
  movil int(11),
  email varchar(200),
  tipo varchar(100),
  alta date,
  baja date,
  observaciones text,
  PRIMARY KEY(id),
  UNIQUE(id,nombre)
);

DROP TABLE IF EXISTS publicidad;
CREATE OR REPLACE TABLE publicidad(
  id int(11) AUTO_INCREMENT,
  agencia int(11),
  descripcion varchar(100),
  contenido varchar(200),
  fecha date,
  importe float(4,2),
  observaciones text,
  PRIMARY KEY(id),
  CONSTRAINT publicidad_agencia FOREIGN KEY (agencia) REFERENCES publicidad(id)
);

DROP TABLE IF EXISTS clientes;
CREATE OR REPLACE TABLE clientes(
  id int(11) AUTO_INCREMENT,
  nombre varchar(100),
  rs varchar(100),
  dir varchar(200),
  cp int(11),
  poblacion varchar(100),
  movil int(11),
  email varchar(200),
  sector varchar(100),
  alta date,
  baja date,
  observaciones text,
  PRIMARY KEY(id)
);

DROP TABLE IF EXISTS compran;
CREATE OR REPLACE TABLE compran(
  id int(11) AUTO_INCREMENT,
  cliente int(11),
  publicidad int(11),
  fecha date,
  expiracion date,
  observaciones text,
  PRIMARY KEY(id),
  CONSTRAINT compran_cliente FOREIGN KEY(cliente) REFERENCES clientes(id),
  CONSTRAINT compran_publi FOREIGN KEY(publicidad) REFERENCES publicidad(id)
);


DROP TABLE IF EXISTS productos;
CREATE OR REPLACE TABLE productos(
  id int(11) AUTO_INCREMENT,
  referencia varchar(30),
  tipo varchar(100),
  descripcion text,
  PRIMARY KEY(id)
  );

DROP TABLE IF EXISTS colaboradores;
CREATE OR REPLACE TABLE colaboradores(
  id int(11) AUTO_INCREMENT,
  nombre varchar(100),
  rs varchar(100),
  dir varchar(200),
  cp int(11),
  pob varchar(100),
  movil int(11),
  email varchar(100),
  tipo varchar(100),
  alta date,
  baja date,
  observaciones text,
  PRIMARY KEY(id)
  );

DROP TABLE IF EXISTS proveedores;
CREATE OR REPLACE TABLE proveedores(
  id int(11) AUTO_INCREMENT,
  nombre varchar(100),
  rs varchar(100),
  dir varchar(200),
  cp int(11),
  pob varchar(100),
  movil int(11),
  email varchar(100),
  alta date,
  baja date,
  observaciones text,
  PRIMARY KEY(id),
  UNIQUE(id,nombre)
  );

DROP TABLE IF EXISTS visualiza;
CREATE OR REPLACE TABLE visualiza(
  id int(11) AUTO_INCREMENT,
  producto int(11),
  publicidad int(11),
  colaborador int(11),
  cantidad int(5),
  fincontrato date,
  PRIMARY KEY(id),
  CONSTRAINT visualiza_producto FOREIGN KEY(producto) REFERENCES productos(id),
  CONSTRAINT visualiza_publi FOREIGN KEY(publicidad) REFERENCES publicidad(id),
  CONSTRAINT visualiza_colabora FOREIGN KEY(colaborador) REFERENCES colaboradores(id)
  );

drop TABLE IF EXISTS fabrican;
CREATE OR REPLACE TABLE fabrican(
  id int(11) AUTO_INCREMENT,
  proveedor varchar(100),
  producto int(11),
  fecha date,
  cantidad int(5),
  importe float(4,2),
  PRIMARY KEY(id),
  CONSTRAINT fabrica_producto FOREIGN KEY(producto) REFERENCES productos(id)
  );

