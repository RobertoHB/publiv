<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\proveedores;

/**
 * ProveedoresSearch represents the model behind the search form of `app\models\proveedores`.
 */
class ProveedoresSearch extends proveedores
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cp', 'movil'], 'integer'],
            [['nombre', 'rs', 'dir', 'pob', 'email', 'alta', 'baja', 'observaciones'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = proveedores::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cp' => $this->cp,
            'movil' => $this->movil,
            'alta' => $this->alta,
            'baja' => $this->baja,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'rs', $this->rs])
            ->andFilterWhere(['like', 'dir', $this->dir])
            ->andFilterWhere(['like', 'pob', $this->pob])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones]);

        return $dataProvider;
    }
}
