<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agencias".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $rs
 * @property string|null $dir
 * @property int|null $cp
 * @property string|null $poblacion
 * @property int|null $movil
 * @property string|null $email
 * @property string|null $tipo
 * @property string|null $alta
 * @property string|null $baja
 * @property string|null $observaciones
 */
class Agencias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agencias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cp', 'movil'], 'integer'],
            [['alta', 'baja'], 'safe'],
            [['observaciones'], 'string'],
            [['nombre', 'rs', 'poblacion', 'tipo'], 'string', 'max' => 100],
            [['dir', 'email'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'rs' => 'Rs',
            'dir' => 'Dir',
            'cp' => 'Cp',
            'poblacion' => 'Poblacion',
            'movil' => 'Movil',
            'email' => 'Email',
            'tipo' => 'Tipo',
            'alta' => 'Alta',
            'baja' => 'Baja',
            'observaciones' => 'Observaciones',
        ];
    }
    
      public function afterFind() {
        parent::afterFind();
        $this->alta=Yii::$app->formatter->asDate($this->alta, 'php:d-m-Y');
        if($this->baja != Null){
            $this->baja=Yii::$app->formatter->asDate($this->baja, 'php:d-m-Y');
        }else{
            $this->baja = " ";
        }    
    }

    
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          if($this->alta != Null){
            $this->alta=Yii::$app->formatter->asDate($this->alta, 'php:Y-m-d');
            return true;
          }else{
              $this->alta = " ";
              return true;
          }
         
        //$this->baja=Yii::$app->formatter->asDate($this->baja, 'php:Y-m-d');
          if($this->baja != Null){
              $this->baja=Yii::$app->formatter->asDate($this->baja, 'php:Y-m-d');
              return true;
          }else{
              $this->baja = " ";
              return true;
           }    
      
        
    }
}
