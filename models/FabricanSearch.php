<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\fabrican;

/**
 * FabricanSearch represents the model behind the search form of `app\models\fabrican`.
 */
class FabricanSearch extends fabrican
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'producto', 'cantidad'], 'integer'],
            [['proveedor', 'fecha'], 'safe'],
            [['importe'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = fabrican::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'producto' => $this->producto,
            'fecha' => $this->fecha,
            'cantidad' => $this->cantidad,
            'importe' => $this->importe,
        ]);

        $query->andFilterWhere(['like', 'proveedor', $this->proveedor]);

        return $dataProvider;
    }
}
