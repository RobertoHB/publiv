<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "colaboradores".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $rs
 * @property string|null $dir
 * @property int|null $cp
 * @property string|null $pob
 * @property int|null $movil
 * @property string|null $email
 * @property string|null $tipo
 * @property string|null $alta
 * @property string|null $baja
 * @property string|null $observaciones
 *
 * @property Visualiza[] $visualizas
 */
class colaboradores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'colaboradores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cp', 'movil'], 'integer'],
            [['alta', 'baja'], 'safe'],
            [['observaciones'], 'string'],
            [['nombre', 'rs', 'pob', 'email', 'tipo'], 'string', 'max' => 100],
            [['dir'], 'string', 'max' => 200],
            [['tipo'], 'string', 'max' => 100],
            [['tipoCliente'], 'string', 'max' => 100],
            [['provincia'], 'string', 'max' => 100],
            [['afluencia'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'rs' => 'Rs',
            'dir' => 'Dir',
            'cp' => 'Cp',
            'pob' => 'Pob',
            'movil' => 'Movil',
            'email' => 'Email',
            'tipo' => 'Tipo',
            'tipoCliente' => 'TipoCliente',
            'provincia' => 'Provincia',
            'afluencia' => 'Afluencia',
            'alta' => 'Alta',
            'baja' => 'Baja',
            'observaciones' => 'Observaciones',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVisualizas()
    {
        return $this->hasMany(Visualiza::className(), ['colaborador' => 'id']);
    }
}
