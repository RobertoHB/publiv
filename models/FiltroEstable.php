<?php

namespace app\models;



use Yii;

/**
 * This is the model class for table "recibos".
 *
 * @property int $id
 * @property int $matricula
 * @property string $emision
 * @property int $mes
 * @property int $año
 * @property int $estado
 * @property int $reducido
 * @property double $importe
 *
 * @property Matriculas $matricula0
 */

 
 
class filtroEstable extends colaboradores{
    public $tipoCliente;
    public $tipoEstablecimiento;
    public $provincia;
    public $afluencia;
    public $activo;
    /**
     * {@inheritdoc}
     */
  

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipoCliente'], 'string'],
            [['tipoEstablecimiento'], 'string'],
            [['provincia'], 'string'],
            [['afluencia'], 'string'],
            [['activo'], 'boolean'],   
       ];        
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
         $labels = ['tipoCliente' => 'tipoCliente', 'tipoEstablecimiento' => 'tipoEstablecimiento', 'provincia' => 'provincia', 'afluencia' => 'afluencia', 'activo' => 'activo'];
        return array_merge(parent::attributeLabels(), $labels);
       
    }

     
   
}
