<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $id
 * @property string|null $referencia
 * @property string|null $tipo
 * @property string|null $descripcion
 *
 * @property Fabrican[] $fabricans
 * @property Visualiza[] $visualizas
 */
class productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion'], 'string'],
            [['referencia'], 'string', 'max' => 30],
            [['tipo'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'referencia' => 'Referencia',
            'tipo' => 'Tipo',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFabricans()
    {
        return $this->hasMany(Fabrican::className(), ['producto' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVisualizas()
    {
        return $this->hasMany(Visualiza::className(), ['producto' => 'id']);
    }
}
