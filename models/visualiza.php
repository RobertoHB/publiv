<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visualiza".
 *
 * @property int $id
 * @property int|null $producto
 * @property int|null $publicidad
 * @property int|null $colaborador
 * @property int|null $cantidad
 * @property string|null $fincontrato
 *
 * @property Colaboradores $colaborador0
 * @property Productos $producto0
 * @property Publicidad $publicidad0
 */
class visualiza extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visualiza';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['producto', 'publicidad', 'colaborador', 'cantidad'], 'integer'],
            [['fincontrato'], 'safe'],
            [['colaborador'], 'exist', 'skipOnError' => true, 'targetClass' => Colaboradores::className(), 'targetAttribute' => ['colaborador' => 'id']],
            [['producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['producto' => 'id']],
            [['publicidad'], 'exist', 'skipOnError' => true, 'targetClass' => Publicidad::className(), 'targetAttribute' => ['publicidad' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'producto' => 'Producto',
            'publicidad' => 'Publicidad',
            'colaborador' => 'Colaborador',
            'cantidad' => 'Cantidad',
            'fincontrato' => 'Fincontrato',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColaborador0()
    {
        return $this->hasOne(Colaboradores::className(), ['id' => 'colaborador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto0()
    {
        return $this->hasOne(Productos::className(), ['id' => 'producto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicidad0()
    {
        return $this->hasOne(Publicidad::className(), ['id' => 'publicidad']);
    }
    
     public function afterFind() {
        parent::afterFind();
        $this->fincontrato=Yii::$app->formatter->asDate($this->fincontrato, 'php:d-m-Y');
        
    }

    
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          if($this->fincontrato != Null){
            $this->fincontrato=Yii::$app->formatter->asDate($this->fincontrato, 'php:Y-m-d');
            return true;
          }else{
              $this->fincontrato = " ";
              return true;
          }
        
    }
}
