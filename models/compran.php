<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compran".
 *
 * @property int $id
 * @property int|null $cliente
 * @property int|null $publicidad
 * @property string|null $fecha
 * @property string|null $expiracion
 * @property string|null $observaciones
 *
 * @property Clientes $cliente0
 * @property Publicidad $publicidad0
 */
class compran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente', 'publicidad'], 'integer'],
            [['fecha', 'expiracion'], 'safe'],
            [['observaciones'], 'string'],
            [['cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente' => 'id']],
            [['publicidad'], 'exist', 'skipOnError' => true, 'targetClass' => Publicidad::className(), 'targetAttribute' => ['publicidad' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cliente' => 'Cliente',
            'publicidad' => 'Publicidad',
            'fecha' => 'Fecha',
            'expiracion' => 'Expiracion',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente0()
    {
        return $this->hasOne(Clientes::className(), ['id' => 'cliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicidad0()
    {
        return $this->hasOne(Publicidad::className(), ['id' => 'publicidad']);
    }
    
    
     public function afterFind() {
        parent::afterFind();
        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d-m-Y');
        
        if($this->expiracion != Null){
            $this->expiracion=Yii::$app->formatter->asDate($this->expiracion, 'php:d-m-Y');
        }else{
            $this->expiracion = " ";
        }    
    }

    
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          if($this->fecha != Null){
            $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:Y-m-d');
            return true;
          }else{
              $this->fecha = " ";
              return true;
          }
         
        //$this->baja=Yii::$app->formatter->asDate($this->baja, 'php:Y-m-d');
          if($this->expiracion != Null){
              $this->expiracion=Yii::$app->formatter->asDate($this->expiracion, 'php:Y-m-d');
              return true;
          }else{
              $this->expiracion = " ";
              return true;
           }    
      
        
    }
}
