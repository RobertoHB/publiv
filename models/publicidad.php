<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publicidad".
 *
 * @property int $id
 * @property int|null $agencia
 * @property string|null $descripcion
 * @property string|null $contenido
 * @property string|null $fecha
 * @property float|null $importe
 * @property string|null $observaciones
 *
 * @property Compran[] $comprans
 * @property publicidad $agencia0
 * @property publicidad[] $publicidads
 * @property Visualiza[] $visualizas
 */
class publicidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'publicidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['agencia'], 'integer'],
            [['fecha'], 'safe'],
            [['importe'], 'number'],
            [['observaciones'], 'string'],
            [['descripcion'], 'string', 'max' => 100],
            [['contenido'], 'string', 'max' => 200],
//            [['agencia'], 'unique', 'targetAttribute' => ['agencia']],
            [['agencia'], 'exist', 'skipOnError' => true, 'targetClass' => publicidad::className(), 'targetAttribute' => ['agencia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agencia' => 'Agencia',
            'descripcion' => 'Descripcion',
            'contenido' => 'Contenido',
            'fecha' => 'Fecha',
            'importe' => 'Importe',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComprans()
    {
        return $this->hasMany(Compran::className(), ['publicidad' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgencia0()
    {
        return $this->hasOne(publicidad::className(), ['id' => 'agencia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicidads()
    {
        return $this->hasMany(publicidad::className(), ['agencia' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVisualizas()
    {
        return $this->hasMany(Visualiza::className(), ['publicidad' => 'id']);
    }
    
    
      public function afterFind() {
        parent::afterFind();
        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d-m-Y');
       
    }

    
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          if($this->fecha != Null){
            $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:Y-m-d');
            return true;
          }else{
              $this->fecha = " ";
              return true;
          }
    }
}
