<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fabrican".
 *
 * @property int $id
 * @property string|null $proveedor
 * @property int|null $producto
 * @property string|null $fecha
 * @property int|null $cantidad
 * @property float|null $importe
 *
 * @property Productos $producto0
 */
class fabrican extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fabrican';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['producto', 'cantidad'], 'integer'],
            [['fecha'], 'safe'],
            [['importe'], 'number'],
            [['proveedor'], 'string', 'max' => 100],
            [['producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['producto' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proveedor' => 'Proveedor',
            'producto' => 'Producto',
            'fecha' => 'Fecha',
            'cantidad' => 'Cantidad',
            'importe' => 'Importe',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto0()
    {
        return $this->hasOne(Productos::className(), ['id' => 'producto']);
    }
    
    
     public function afterFind() {
        parent::afterFind();
        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d-m-Y');
        
    }

    
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          if($this->fecha != Null){
            $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:Y-m-d');
            return true;
          }else{
              $this->fecha = " ";
              return true;
          }
        
    }
    
    
    
}
