<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\visualiza;

/**
 * VisualizaSearch represents the model behind the search form of `app\models\visualiza`.
 */
class VisualizaSearch extends visualiza
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'producto', 'publicidad', 'colaborador', 'cantidad'], 'integer'],
            [['fincontrato'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = visualiza::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'producto' => $this->producto,
            'publicidad' => $this->publicidad,
            'colaborador' => $this->colaborador,
            'cantidad' => $this->cantidad,
            'fincontrato' => $this->fincontrato,
        ]);

        return $dataProvider;
    }
}
