<?php

namespace app\controllers;

use Yii;
use app\models\colaboradores;
use app\models\ColaboradoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\filtroEstable;
use app\models\visualiza;


/**
 * ColaboradoresController implements the CRUD actions for colaboradores model.
 */
class ColaboradoresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all colaboradores models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ColaboradoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single colaboradores model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new colaboradores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new colaboradores();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing colaboradores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing colaboradores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the colaboradores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return colaboradores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = colaboradores::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
    
 public function actionFiltrar(){
     
 
         $model = new filtroEstable();
       
     
     return $this->render('establecimientos',['model' => $model]);
   
 }
 

 
 public function actionEstablecimientos(){
     $model = new filtroEstable();
    
    $model->load(\Yii::$app->request->post());


if ($model->tipoEstablecimiento || $model->tipoCliente || $model->afluencia || $model->provincia || $model->activo) {
       
    
          $tipoEstablecimiento = $model->tipoEstablecimiento;
          $tipoCliente = $model->tipoCliente; 
          $afluencia = $model->afluencia;
          $provincia = $model->provincia;
          $activo = $model->activo;
          
//          if ($tipoEstablecimiento<>""){
//              $filtro = " where tipo = '$tipoEstablecimiento'";
//          } 
              if ($activo <>""){
                   $filtro = " and tipo = '$tipoEstablecimiento'";
              }else{
                  $filtro = " WHERE tipo = '$tipoEstablecimiento'";
              }
          
           if ($tipoCliente<>""){
              $filtro .= " and tipoCliente = '$tipoCliente'";
          } 
           if ($afluencia<>""){
              $filtro .= " and afluencia = '$afluencia'";
          } 
            if ($provincia<>""){
              $filtro .= " and provincia = '$provincia'";
          } 
          if ($activo <>""){
              if($activo == 1){
                  //activo
                  $sql = "
                        SELECT cola.id id,cola.nombre nombre, cola.dir dir, cola.email email, cola.pob pob FROM colaboradores cola LEFT join visualiza visu ON cola.id = visu.colaborador WHERE visu.colaborador IS NULL$filtro
                        UNION
                        SELECT cola.id id,cola.nombre nombre, cola.dir dir, cola.email email, cola.pob pob FROM colaboradores cola JOIN visualiza visu ON cola.id = visu.colaborador WHERE visu.fincontrato < CURRENT_DATE()$filtro";
          
              }else{
                  //inactivo
                  $sql = "SELECT cola.id id,cola.nombre nombre, cola.dir dir, cola.email email, cola.pob pob FROM colaboradores cola join visualiza visu ON cola.id = visu.colaborador WHERE visu.fincontrato > CURRENT_DATE()$filtro";
                  
              }
          }else{
              $sql = "SELECT cola.id id,cola.nombre nombre, cola.dir dir, cola.email email, cola.pob pob FROM colaboradores cola
                       $filtro";
          }
          
//           if ($activo=0){
//              $filtro .= " and fecha = ";
//          } 
//           if ($activo=1){
//              $filtro .= " and fecha = ";
//          } 
//          echo("Tipo de establecimiento:".$tipoEstablecimiento);
//          echo("Tipo de Cliente: ".$tipocliente);
//          echo("Afluencia: ".$afluencia);
//          echo("Provincia: ".$provincia);
//          exit;
//          $dataProvider = new SqlDataProvider([
                              
               
          
//          $datos = Colaboradores::find()
//                  ->all();
        
         $datos = new SqlDataProvider([
                    'sql' => $sql]);
//         $datos1 = $datos->getModels();
               
          
              
        return $this->render('establecimientos', [
            "dataProvider" => $datos,
            "model"=>$model,
            "campos"=>['id','nombre','dir','email','pob'],
            "titulo"=>"Listado de establecimientos",
            "enunciado"=>"Establecimientos",
           
          ]);
       
      }else{
          $dataProvider = Null;
          
          return $this->render('establecimientos',['model' => $model,"dataProvider" => $dataProvider]);
    }
}  
}
